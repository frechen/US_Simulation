%% ========================================================================
% PRELIMINARY STEPS
% Clear variables
clear
clc

%% ------------------------------------------------------------------------
% DEFINITION OF INPUT DATA
% Definition of grid properties
x=58e-3;                            %[m] Width of the sample
y=18e-3;                            %[m] Height of the sample
z=18e-3;                            %[m] Depth of the sample
dx = 0.075e-3;                      %[m] grid point spacing in the x direction
dy = dx;                            %[m] grid point spacing in the y direction
dz = dx;                            %[m] grid point spacing in the z direction
CFL = 0.1;                          % CFL number

% Conversions of values to grid points
x = ceil(x/dx);                     % [grid point] Width of the sample
y = ceil(y/dy);                     % [grid point] Height of the sample
z = ceil(z/dz);                     % [grid point] Depth of the sample
PML_size=20;                        % [grid points] Area required for the PML

% Optimize Nx,Ny,Nz values to low prime numbers (file contains all numbers,
% that contain low prime factors)
load prime_numbers.mat
    
% Definition of computational grid
Nx = prime_num(find(prime_num>=x+2*PML_size,1));   % Number of grid points in the x (row) direction
Ny = prime_num(find(prime_num>=y+2*PML_size,1));   % Number of grid points in the x (row) direction
Nz = prime_num(find(prime_num>=z+2*PML_size,1));   % Number of grid points in the x (row) direction
% Recalculate x,y,z
x = Nx-2*PML_size;
y = Ny-2*PML_size;
z = Nz-2*PML_size;

kgrid = makeGrid(Nx, dx, Ny, dy);

%% Medium Properties - Part 1
% Materials
% Acoustic properties of water (at 50�C)
sound_speed = 1543;         % [m/s] Sound speed of the medium
density = 988;             % [kg/m^3]
alpha_coef_water = 0.0022;      %[dB/(MHz^b*cm)] Power law absorption prefactor (a)
b_water = 2;             %Power law absorption exponent (b)
% Acoustic properties of Material 1
sound_speed_1 = 1770;      % [m/s] Sound speed of the medium
density_1 = 894;           % [kg/m^3]
alpha_coef_1 = 4;        %[dB/(MHz^b*cm)] Power law absorption prefactor (a)
b_1 = 1.18;                 %Power law absorption exponent (b)
% Acoustic properties of Material 2
sound_speed_2 = 1680;        % [m/s] Sound speed of the medium
density_2 = 1109;            % [kg/m^3]
alpha_coef_2 = 8.4;          %[dB/(MHz^b*cm)] Power law absorption prefactor (a)
b_2 = 0.99;                    %Power law absorption exponent (b)
% Acoustic properties of Copper
sound_speed_3 = 4660;          % [m/s] Sound speed of the medium
density_3 = 8900;              % [kg/m^3]
alpha_coef_3 = 0.01;         %[dB/(MHz^b*cm)] Power law absorption prefactor (a)
b_3 = 2;                    %Power law absorption exponent (b)



%% Grid details
% Definition of sample
dia_out = 84e-3;                %[mm]Outer diameter of the sample
dia_out = floor(dia_out/dx);    %[grid point]Outer diameter of the sample
M1_out = 1.7e-3;                %[mm]Thickness of outer layer
M1_out = floor(M1_out/dx);      %[grid point]Thickness of outer layer
M1_in = 44.6e-3;                %[mm]Diameter of inner area
M1_in = floor(M1_in/dx);        %[grid point]Diameter of inner area
dia_in = 41.6e-3;               %[mm]Inner diameter
dia_in = floor(dia_in/dx);      %[grid point]Inner diameter

load('mask');
mask = imresize(mask,[dia_in dia_in]);
mask = wextend(2,'zpd',mask,floor((dia_out-dia_in)/2));

% Definition of mask
material_mask = makeDisc(dia_out,dia_out,dia_out/2,dia_out/2,dia_out/2);
material_mask = material_mask + makeDisc(dia_out,dia_out,dia_out/2,dia_out/2,dia_out/2-M1_out);
material_mask = material_mask + makeDisc(dia_out,dia_out,dia_out/2,dia_out/2,floor(M1_in/2));
material_mask(mask==1)=5;

% Definition of sound speed mask
mask_speed = zeros(dia_out,dia_out);
mask_speed(material_mask==0) = sound_speed;         % Water
mask_speed(material_mask==1) = sound_speed_2;       % M1
mask_speed(material_mask==2) = sound_speed_1;       % M2
mask_speed(material_mask==3) = sound_speed_2;       % M1
mask_speed(material_mask==5) = sound_speed_3;       % Copper

% Definition of sound speed mask
mask_density = zeros(dia_out,dia_out);
mask_density(material_mask==0) = density;           % Water
mask_density(material_mask==1) = density_2;         % M1
mask_density(material_mask==2) = density_1;         % M2
mask_density(material_mask==3) = density_2;         % M1
mask_density(material_mask==5) = density_3;         % Copper

% Definition of medium alpha power (must be scalar)
medium.alpha_power = 2;
tone_burst_freq = 5e6;              % [Hz]

% Definition of alpha power factor
power_factor = zeros(dia_out,dia_out);
power_factor(material_mask==0) = alpha_coef_water*(tone_burst_freq*1e-6)^(b_water-medium.alpha_power);  %Water
power_factor(material_mask==1) = alpha_coef_2*(tone_burst_freq*1e-6)^(b_2-medium.alpha_power);          %M1
power_factor(material_mask==2) = alpha_coef_1*(tone_burst_freq*1e-6)^(b_1-medium.alpha_power);          %M2
power_factor(material_mask==3) = alpha_coef_2*(tone_burst_freq*1e-6)^(b_2-medium.alpha_power);          %M1
power_factor(material_mask==5) = alpha_coef_3*(tone_burst_freq*1e-6)^(b_3-medium.alpha_power);          %Copper

% Definition of medium sound speed element
medium.sound_speed= ones(Nx,Ny,Nz)*sound_speed;
medium.density= ones(Nx,Ny,Nz)*density;
medium.alpha_coeff= ones(Nx,Ny,Nz)*alpha_coef_water;

% Selection of the part of the sample to be scanned
t_start_ref = 41.85e-6;                         %[s] Time of flight of the first reflection (lab results)
dist = sound_speed*t_start_ref/2;         %[m]Distance between transducer and sample
dist = floor(dist/dx)+PML_size+1;   %[grid point]Distance between transducer and sample
for k=1:Nz
    medium.sound_speed(dist:end,:,k)=...
        mask_speed(1:Nx-dist+1,floor(dia_out/2-Ny/2)+1:floor(dia_out/2+Ny/2));
    medium.density(dist:end,:,k)=...
        mask_density(1:Nx-dist+1,floor(dia_out/2-Ny/2)+1:floor(dia_out/2+Ny/2));
    medium.alpha_coeff(dist:end,:,k)=...
        power_factor(1:Nx-dist+1,floor(dia_out/2-Ny/2)+1:floor(dia_out/2+Ny/2));
end

medium.sound_speed = medium.sound_speed(:,:,1);
medium.density = medium.density(:,:,1);
medium.alpha_coeff = medium.alpha_coeff(:,:,1);
medium.sound_speed_ref = sound_speed;
%medium.alpha_mode = 'no_absorption';

%% Acoustic Source
% Geometry
% Define geometry characteristics
source_length = 10e-3;                  %[m]
source_x_pos = floor(PML_size+1);       %[grid point] Initial position in y
source_y_pos = floor(Ny/2);             %[grid point] Initial position in x
source_z_pos = floor(Nz/2);             %[grid point] Initial position in z
% Calculate and locate source
source_mask=makeDisc(Ny,Nz,source_y_pos,source_z_pos,floor(source_length/(2*dy)));
source.p_mask = zeros(Nx,Ny);
source.p_mask(source_x_pos,source_y_pos-floor(source_length/(2*dy)):source_y_pos+floor(source_length/(2*dy))) = 1;

% Create the time array (necessary to simulate the source)
t_end = 2*x*dx/min(min(min(medium.sound_speed)))+2e-6;  %[s] Total time of simulation
kgrid.t_array = makeTime(kgrid,medium.sound_speed,CFL,t_end);   % Calculation of time step

% Source acoustic characteristics (sinusoidal)
tone_burst_cycles = 4;                                                  % Number of cyles
tone_length = 2*floor(tone_burst_cycles*1/(tone_burst_freq*kgrid.dt));  % Vector length to simulate correctly the signal
source.p = toneBurst(1/kgrid.dt, tone_burst_freq, tone_burst_cycles, ...
        'SignalLength', tone_length);                                   % Pressure vector
source.p = filterTimeSeries(kgrid, medium, source.p);                   % Filtering (remove undesirable frequencies)
p_fac = 1/max(source.p);                                                % Compensation factor of amplitude loss
source.p = source.p*p_fac*100;                                              % Adjustment of amplitude
source.p_mode = 'dirichlet';

%% Measurement sensor
sensor.mask = source.p_mask;
recordDist = 2.5e-3;
sensor.mask(floor(dist-recordDist/kgrid.dx),PML_size+1:Ny-PML_size) = 1;

%The sensor mask will be defined inside the loop

% Definition of measured variables
sensor.record = {'p'};

%% Previous calculations
% Define the input arguments
input_args = {'PlotPML', false, 'DataCast', 'single', 'PlotSim', false};

%% Simulation

% A-scan simulation
t1 = tic;
sensor_data = kspaceFirstOrder2D(kgrid, medium, source, sensor, input_args{:});
simulationTime = toc(t1);

% Save
save([mfilename '.mat'],'kgrid','medium','sensor','source','sensor_data','simulationTime', '-v7.3')


%% Evaluation
sIdx = find(sensor.mask);
[sIdx1,sIdx2] = ind2sub(size(sensor.mask),sIdx);
scan = mean(sensor_data.p(sIdx1<100,:));
figure
plot(kgrid.t_array*1e6,-scan./min(scan(5000:end)))
xlim([41 70])
xlabel('Time of flight in \mus')
ylabel('Amplitude in a.u.')
grid on
title('2D - With Absorption')
