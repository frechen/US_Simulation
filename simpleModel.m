% ccc;
t0 = linspace(-2e-6,2e-6,1000);

% Basic pulse shape
beta = 5e13;
f0 = 5e6;
P0 = -exp(-beta.*t0.^2).*cos(2*pi*f0*t0);

% Acoustic properties of Water
sound_speed = 1543;         % [m/s] Sound speed of the medium
density = 988;             % [kg/m^3]
alpha_coef_water = 0.0022;      %[dB/(MHz^b*cm)] Power law absorption prefactor (a)
b_water = 2;             %Power law absorption exponent (b)
% Acoustic properties of Material 1
sound_speed_1 = 1770;      % [m/s] Sound speed of the medium
density_1 = 894;           % [kg/m^3]
alpha_coef_1 = 4;        %[dB/(MHz^b*cm)] Power law absorption prefactor (a)
b_1 = 1.18;                 %Power law absorption exponent (b)
% Acoustic properties of Material 2
sound_speed_2 = 1680;        % [m/s] Sound speed of the medium
density_2 = 1109;            % [kg/m^3]
alpha_coef_2 = 8.4;          %[dB/(MHz^b*cm)] Power law absorption prefactor (a)
b_2 = 0.99;                    %Power law absorption exponent (b)
% Acoustic properties of Copper
sound_speed_3 = 4660;          % [m/s] Sound speed of the medium
density_3 = 8900;              % [kg/m^3]
alpha_coef_3 = 0.01;         %[dB/(MHz^b*cm)] Power law absorption prefactor (a)
b_3 = 2;                    %Power law absorption exponent (b)

% Sound Impedance for each layer
Z(1) = sound_speed*density;
Z(2) = sound_speed_2*density_2;
Z(3) = sound_speed_1*density_1;
Z(4) = sound_speed_2*density_2;
Z(5) = sound_speed_3*density_3;

% Sound Velocity for each layer
c(1) = sound_speed;
c(2) = sound_speed_2;
c(3) = sound_speed_1;
c(4) = sound_speed_2;
c(5) = sound_speed_3;

% Layer thickness
S(1) = 32.2e-3;
S(2) = 1.7e-3;
S(3) = 18e-3;
S(4) = 1.5e-3;
S(5) = 20.8e-3;

% Time of flight for each segment
ToF(1) = 2*S(1)/c(1)*1e6;
ToF(2) = 2*S(2)/c(2)*1e6;
ToF(3) = 2*S(3)/c(3)*1e6;
ToF(4) = 2*S(4)/c(4)*1e6;
ToF(5) = 2*S(5)/c(5)*1e6;

mu(1) = alpha_coef_water*(f0*1e-6)^b_water*100/8.686;
mu(2) = alpha_coef_2*(f0*1e-6)^b_2*100/8.686;
mu(3) = alpha_coef_1*(f0*1e-6)^b_1*100/8.686;
mu(4) = alpha_coef_2*(f0*1e-6)^b_2*100/8.686;
mu(5) = alpha_coef_3*(f0*1e-6)^b_3*100/8.686;

T(1) = 1;

% Time Vector
common_time = linspace(0,80e-6,1e6);
ETotal = zeros(size(common_time));

% Calculate pulse propagation 
A0 = 1;

% Loop layers
for i = 1:4
    % Transmission coefficients
    T(i+1) = (4*Z(i)*Z(i+1))/(Z(i)+Z(i+1))^2;    
    % Reflection
    R(i) = (Z(i+1)-Z(i))/(Z(i)+Z(i+1));
    % Attenuation
    E(i) = A0*prod(T(1:i).*exp(-2.*mu(1:i).*S(1:i))).*R(i);
    t(i) = sum(2*S(1:i)./c(1:i));
    [~,idx]=min(abs(common_time-t(i)));
    % Interface index
    tIdx(i) = idx;
end

ETotal(tIdx) = E;

P0 = interp1(t0+2e-6,P0,common_time,'linear','extrap');

ETotal = conv(P0,ETotal);
ETotal(length(common_time)+1:end) = []; 

% Plot normalized to minimum of 1st pulse and shifted to ToF = 0 for
% comparability
figure
subplot(1,2,1)
plot(common_time*1e6-43.73,ETotal/-min(ETotal))
grid on
xlabel('Time of flight in \mus')
ylabel('Amplitude in a.u.')
xlim([-2 3])
subplot(1,2,2)
plot(common_time*1e6-43.73,ETotal/-min(ETotal))
grid on
xlabel('Time of flight in \mus')
ylabel('Amplitude in a.u.')
xlim([20 28])
